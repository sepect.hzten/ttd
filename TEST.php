<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>

<body>
    <?php
    if (isset($_POST['kirim'])) {
        $foto = $_FILES['foto'];
        $confidence = $_POST['conf'];
        $ekstensi = pathinfo($foto['name'])['extension'];
        $namaFoto = rand(1000, 9999) . '.' . $ekstensi;
        $cekUpload = move_uploaded_file($foto['tmp_name'], 'uploads/' . $namaFoto);

        if ($cekUpload) {
            $cekRender = shell_exec("python3 ./py-cnn/detect.py --weights ./py-cnn/best.pt --img 256 --conf " . $confidence . " --source ./uploads/" . $namaFoto);
            // shell_exec("python3 ./py-cnn/detect.py --weights ./py-cnn/best.pt --img 256 --conf 0.5 --source ./py-cnn/data/images/3070.jpg");
            // if ($cekRender) {

            // echo "<pre>$cekRender</pre>";

    ?>
            <img src="py-cnn/runs/detect/<?= $namaFoto ?>" alt="deteksi" width="600">
        <?php
            // }
        }
    } else {
        ?>
        <form action="" method="post" enctype="multipart/form-data">
            <input type="file" name="foto" id="foto" accept="image/jpg,image/png,image/jpeg">
            <button type="submit" name="kirim">kirim</button>
        </form>
    <?php
    }
    ?>
</body>

</html>