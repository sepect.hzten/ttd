<ul class="menu-inner py-1">
    <!-- Dashboards -->
    <li class="menu-item">
        <a href="./" class="menu-link">
            <i class="menu-icon tf-icons mdi mdi-home-outline"></i>
            <div data-i18n="Dashboards">Dashboards</div>
        </a>
    </li>
    <?php
    if ($_SESSION['jenis'] === 'staf') {
        ?>
    <!-- Front Pages -->
    <li class="menu-item">
        <a href="#" onclick="hapus()" class="menu-link">
            <i class='menu-icon tf-icons mdi mdi-flip-to-front'></i>
            <div data-i18n="Cek Tanda Tangan">Upload Tanda Tangan</div>
        </a>
    </li>

        <?php
    }elseif($_SESSION['jenis'] === 'admin'){
        ?>
    <!-- Front Pages -->
    <li class="menu-item">
        <a href="?halaman=staf" class="menu-link">
            <i class='menu-icon tf-icons mdi mdi-flip-to-front'></i>
            <div data-i18n="Data Staf">Data Staf</div>
        </a>
    </li>
        <?php
    }
    ?>
    <li class="menu-item">
        <a href="./logout.php" class="menu-link">
            <i class="menu-icon tf-icons mdi mdi-logout-variant"></i>
            <div data-i18n="Logout">Logout</div>
        </a>
    </li>
</ul>