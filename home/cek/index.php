<div class="container-xxl flex-grow-1 container-p-y">
    <h4 class="py-3 mb-4">
        <span class="text-black fw-light">Cek Tanda Tangan</span>
    </h4>
    <div class="row gy-4">
        <!-- Gamification Card -->
        <div class="col-md-12 col-lg-12">
            <div class="card">
                <h5 class="card-header">
                    <button class="btn btn-primary" data-bs-toggle="modal" data-bs-target="#add">Input tanda tangan</button>
                </h5>
                <div class="card-datatable table-responsive">
                    <table id="myTable" class="table table-bordered">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Confidence</th>
                                <th>Tanda Tangan yang dicek</th>
                                <th>Hasil Pengenalan Tanda Tanda Tangan</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            $no = 1;
                            $data = mysqli_query($koneksi, "select * from tb_tanda_tangan");
                            foreach ($data as $r) {
                            ?>
                                <tr>
                                    <td><?= $no++; ?></td>
                                    <th><?= $r['confidence'] / 100; ?></th>
                                    <td><img src="../uploads/<?= $r['foto_ttd'] ?>" alt="foto" width="200"></td>
                                    <td>
                                        <a href="../py-cnn/runs/detect/<?= $r['foto_ttd'] ?>" target="_blank">
                                            <img src="../py-cnn/runs/detect/<?= $r['foto_ttd'] ?>" alt="foto" width="200">
                                        </a>
                                    </td>
                                </tr>
                            <?php
                            }
                            ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>


<div class="modal fade" id="add" tabindex="-1" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="modalCenterTitle">Input Tanda Tangan</h4>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <form action="" method="post" enctype="multipart/form-data">
                <div class="modal-body">
                    <div class="row g-2">
                        <div class="col mb-2">
                            <div class="form-floating form-floating-outline">
                                <input type="file" id="foto" class="form-control" name="foto" accept="image/*" required>
                                <label for="emailWithTitle">Foto Tanda Tangan</label>
                            </div>
                        </div>
                    </div>

                    <hr>
                    <div class="row g-2">
                        <div class="col mb-2">
                            <div class="form-floating form-floating-outline">
                                <input type="number" min="1" id="conf" class="form-control" name="conf" required max="100">
                                <label for="emailWithTitle">Masukkan Confidence</label>
                            </div><br>
                            <span class="text-danger text-sm">Nilai Confidence 1 - 100</span>
                        </div>
                    </div>
                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-outline-secondary" data-bs-dismiss="modal">Batal</button>
                    <button type="submit" id="btnA" name="simpan" class="btn btn-primary"><b id="ts">Simpan </b><b id="ts2">Sedang Proses </b>
                        <div id="loader" class="spinner-border ms-4" role="status" aria-hidden="true"></div>
                    </button>
                </div>
            </form>
        </div>
    </div>
</div>


<script src="../assets/vendor/libs/jquery/jquery.js"></script>
<script src="../assets/vendor/js/bootstrap.js"></script>
<script>
    $(document).ready(function() {
        $('#loader').hide();
        $('#ts2').hide();
        $('#btnA').click(function() {
            $('#loader').show();
            $('#ts2').show();
            $('#ts').hide();
        });
    });
</script>

<?php

if (isset($_REQUEST['simpan'])) {
    $foto = $_FILES['foto'];
    $confidence = $_POST['conf'] / 100;

    $ekstensi = pathinfo($foto['name'])['extension'];
    $namaFoto = rand(1000, 9999) . '.' . $ekstensi;

    $cekUpload = move_uploaded_file($foto['tmp_name'], '../uploads/' . $namaFoto);

    if ($cekUpload) {
        mysqli_query($koneksi, "TRUNCATE tb_tanda_tangan");
        $save = mysqli_query($koneksi, "insert into tb_tanda_tangan values (null,'" . $namaFoto . "','" . $_POST['conf'] . "')");
        if ($save) {
            $cekRender = shell_exec("python3 ../py-cnn/detect.py --weights ../py-cnn/best.pt --img 256 --conf " . $confidence . " --source ../uploads/" . $namaFoto);
            // if ($cekRender) {
            echo '
        <script>alert("Deteksi Pola Tanda Tangan Berhasil.");
        location.href="./?halaman=cek";
        </script>';
            // }
        } else {
            echo '
                                                <script>alert("Gagal Ditambahkan.");
                                                history.back();
                                                </script>';
        }
    } else {
        echo '
                                                <script>alert("Gagal upload foto!");
                                                history.back();
                                                </script>';
    }
}
