<?php
if (isset($_REQUEST['halaman'])) {
    $halaman = $_REQUEST['halaman'];
    switch ($halaman) {
        case 'cek';
            include 'cek/index.php';
            break;
        case 'staf';
            include 'staf/index.php';
            break;
    }
} else {
?>
    <div class="container-xxl flex-grow-1 container-p-y">
        <div class="row gy-4">
            <!-- Gamification Card -->
            <div class="col-md-12 col-lg-12">
                <div class="card h-100">
                    <div class="d-flex align-items-end row">
                        <div class="col-md-6 order-2 order-md-1">
                            <div class="card-body">
                                <h4 class="card-title pb-xl-2">Selamat Datang! <?= $_SESSION['nama'] ?>🎉</h4>
                                <p class="mb-0">Pengenalan Pola Keaslian Tanda Tangan (CNN) Lembaran Pengesahan Skripsi Mahasiswa UNDIPA <span class="h6 mb-0"></span>😎.</p>
                            </div>
                        </div>
                        <div class="col-md-6 text-center text-md-end order-1 order-md-2">
                            <div class="card-body pb-0 px-0 px-md-4 ps-0">
                                <img src="../assets/img/illustrations/illustration-john-light.png" height="180" alt="View Profile" data-app-light-img="illustrations/illustration-john-light.png" data-app-dark-img="illustrations/illustration-john-dark.html">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php
}
