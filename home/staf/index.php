<div class="container-xxl flex-grow-1 container-p-y">
    <h4 class="py-3 mb-4">
        <span class="text-black fw-light">Data Staf</span>
    </h4>
    <div class="row gy-4">
        <!-- Gamification Card -->
        <div class="col-md-12 col-lg-12">
            <div class="card">
                <h5 class="card-header">
                    <button class="btn btn-primary" data-bs-toggle="modal" data-bs-target="#add">Tambah Data</button>
                </h5>
                <div class="card-datatable table-responsive">
                    <table id="dataT" class="table table-bordered">
                        <thead>
                        <tr>
                            <th>No</th>
                            <th>Nama Staf</th>
                            <th>Username</th>
                            <th>Aksi</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php
                        $no = 1;
                        $data = mysqli_query($koneksi, "select * from tb_staf");
                        foreach ($data as $r) {
                            ?>
                            <tr>
                                <td><?= $no++; ?></td>
                                <td><?= $r['nama_staf'] ?></td>
                                <td><?= $r['username'] ?></td>
                                <td>
                                    <button type="button" class="btn btn-icon btn-info waves-effect waves-light"
                                            data-bs-toggle="modal" data-bs-target="#edit" data-id="<?= $r['id_staf'] ?>"
                                            data-username="<?= $r['username'] ?>" data-nama="<?= $r['nama_staf'] ?>">
                                        <span class="tf-icons mdi mdi-pencil"></span>
                                    </button>
                                    <button type="button" class="btn btn-icon btn-danger waves-effect waves-light"
                                            data-bs-toggle="modal" data-bs-target="#del"
                                            data-id="<?= $r['id_staf'] ?>">
                                        <span class="tf-icons mdi mdi-trash-can"></span>
                                    </button>
                                </td>
                            </tr>
                            <?php
                        }
                        ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>


<div class="modal fade" id="add" tabindex="-1" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="modalCenterTitle">Tambah Data Staf</h4>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <form action="" method="post">
                <div class="modal-body">
                    <div class="row g-2">
                        <div class="col mb-2">
                            <div class="form-floating form-floating-outline">
                                <input type="text" id="nama" class="form-control" name="nama" placeholder="Nama Staf"
                                       maxlength="50" required>
                                <label for="emailWithTitle">Nama Staf</label>
                            </div>
                        </div>
                    </div>
                    <div class="row g-2">
                        <div class="col mb-2">
                            <div class="form-floating form-floating-outline">
                                <input type="text" id="username" class="form-control" name="username" placeholder="Username"
                                       maxlength="30" required>
                                <label for="emailWithTitle">Username</label>
                            </div>
                        </div>
                    </div>
                    <div class="row g-2">
                        <div class="col mb-2">
                            <div class="form-floating form-floating-outline">
                                <input type="password" id="password" class="form-control" name="password" placeholder="******"
                                       maxlength="20" required>
                                <label for="emailWithTitle">Password</label>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-outline-secondary" data-bs-dismiss="modal">Batal</button>
                    <button type="submit" name="simpan" class="btn btn-primary">Simpan</button>
                </div>
            </form>
        </div>
    </div>
</div>

<div class="modal fade" id="edit" tabindex="-1" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="modalCenterTitle">Edit Data Staf</h4>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <form action="" method="post">
                <input type="hidden" name="id" id="id">
                <div class="modal-body">
                    <div class="row g-2">
                        <div class="col mb-2">
                            <div class="form-floating form-floating-outline">
                                <input type="text" id="nama" class="form-control" name="nama" placeholder="Nama Staf"
                                       maxlength="50" required>
                                <label for="emailWithTitle">Nama Staf</label>
                            </div>
                        </div>
                    </div>
                    <div class="row g-2">
                        <div class="col mb-2">
                            <div class="form-floating form-floating-outline">
                                <input type="text" id="username" class="form-control" name="username" placeholder="Username"
                                       maxlength="30" required>
                                <label for="emailWithTitle">Username</label>
                            </div>
                        </div>
                    </div>
                    <div class="row g-2">
                        <div class="col mb-2">
                            <div class="form-floating form-floating-outline">
                                <input type="password" id="password" class="form-control" name="password" placeholder="******"
                                       maxlength="20">
                                <label for="emailWithTitle">Password</label>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-outline-secondary" data-bs-dismiss="modal">Batal</button>
                    <button type="submit" name="edit" class="btn btn-primary">Update</button>
                </div>
            </form>
        </div>
    </div>
</div>

<div class="modal fade" id="del" tabindex="-1" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="modalCenterTitle">Hapus Data Staf</h4>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <form action="" method="post">
                <input type="hidden" name="id" id="id">
                <div class="modal-body">
                    <h5>Yakin ingin menghapus data ini ?</h5>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-outline-secondary" data-bs-dismiss="modal">Batal</button>
                    <button type="submit" name="hapus" class="btn btn-primary">Hapus</button>
                </div>
            </form>
        </div>
    </div>
</div>


<script src="../assets/vendor/libs/jquery/jquery.js"></script>
<script src="../assets/vendor/js/bootstrap.js"></script>

<script>
    $(document).ready(function () {
        $("#edit").on("show.bs.modal", function (event) {
            let button = $(event.relatedTarget);
            let id = button.data('id');
            let username = button.data('username');
            let nama = button.data('nama');
            let modal = $('#edit');
            modal.find('#id').val(id);
            modal.find('#username').val(username);
            modal.find('#nama').val(nama);
        });
        $("#del").on("show.bs.modal", function (event) {
            let button = $(event.relatedTarget);
            let id = button.data('id');
            let modal = $('#del');
            modal.find('#id').val(id);

        });
    });
</script>

<?php

if (isset($_REQUEST['simpan'])) {
    $username = $_REQUEST['username'];
    $password = $_REQUEST['password'];
    $nama = $_REQUEST['nama'];

    $cek = mysqli_num_rows(mysqli_query($koneksi, "select * from tb_staf where username = '" . $tb_staf . "'"));
    if ($cek <= 0) {
        $save = mysqli_query($koneksi, "insert into tb_staf values (null,'$nama','$username','$password')");
        if ($save) {
            echo '
                                                <script>alert("Berhasil Ditambahkan.");
                                                location.href="./?halaman=staf";
                                                </script>';
        } else {
            echo '
                                                <script>alert("Gagal Ditambahkan.");
                                                history.back();
                                                </script>';
        }
    } else {
        echo '
                                                <script>alert("Username sudah digunakan!");
                                                history.back();
                                                </script>';
    }
}

if (isset($_REQUEST['edit'])) {
    $id = $_REQUEST['id'];
    $username = $_REQUEST['username'];
    $password = $_REQUEST['password'];
    $nama = $_REQUEST['nama'];

    $cek = mysqli_num_rows(mysqli_query($koneksi, "select * from tb_staf where username = '" . $username . "' and id_staf != '$id'"));
    if ($cek <= 0) {
        if ($password !== "") {
            
        $save = mysqli_query($koneksi, "update tb_staf set username = '$username',nama_staf = '$nama',password = '$password' where id_staf = '$id'");
        if ($save) {
            echo '
                                                <script>alert("Berhasil diubah.");
                                                location.href="./?halaman=staf";
                                                </script>';
        } else {
            echo '
                                                <script>alert("Gagal Ditambahkan.");
                                                history.back();
                                                </script>';
        }        
    }else{

        $save = mysqli_query($koneksi, "update tb_staf set username = '$username',nama_staf = '$nama' where id_staf = '$id'");
        if ($save) {
            echo '
                                                <script>alert("Berhasil diubah.");
                                                location.href="./?halaman=staf";
                                                </script>';
        } else {
            echo '
                                                <script>alert("Gagal Ditambahkan.");
                                                history.back();
                                                </script>';
        }
    }
    } else {
        echo '
                                                <script>alert("Username sudah digunakan!");
                                                history.back();
                                                </script>';
    }
}

if (isset($_REQUEST['hapus'])) {
    $id = $_REQUEST['id'];

        $hapus = mysqli_query($koneksi, "delete from tb_staf where id_staf = '" . $id . "'");
        if ($hapus) {
            echo '
                                                <script>alert("Berhasil dihapus.");
                                                location.href="./?halaman=staf";
                                                </script>';
        } else {
            echo '
                                                <script>alert("Gagal Ditambahkan.");
                                                history.back();
                                                </script>';
        }
}